const express = require('express');
const axios = require('axios');
const NeDB = require('nedb');

const routes = express.Router();
const usersDB = new NeDB({ autoload: true });

usersDB.find({}, async err => {
    if (err) console.log(err);
    const response = await axios.get('https://jsonplaceholder.typicode.com/users')
    usersDB.insert(response.data)
});

routes.use((req, res, next) => {
    // Definir as regras para o log

    next();
});

routes.get('/', (req, res) => {
    return res.json({
        routes: ['/websites', '/users', '/suite']
    });
});

routes.get('/users', (req, res) => {
    usersDB.find({}, { name: 1, email: 1, company: 1, _id: 0 }, (err, users) => {
        if (err) return res.status(500).json({ err });

        return res.json(users.sort((a, b) => a.name.localeCompare(b.name)));
    });
});

routes.get('/websites', (req, res) => {
    usersDB.find({}, { website: 1, _id: 0 }, (err, users) => {
        if (err) return res.status(500).json({ err });

        return res.json(users.map(user => user.website));
    });
});

routes.get('/suite', (req, res) => {
    usersDB.find({}, { _id: 0 }, (err, users) => {
        if (err) return res.status(500).json({ err });

        return res.json(users.sort((a, b) => a.id - b.id).filter(user => /suite/gi.test(user.address.suite)));
    });
});

module.exports = routes;